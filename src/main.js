import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "animate.css";
import VueComp from "@vue/composition-api";
import VueFirestore from "vue-firestore";
import CommentGrid from "vue-comment-grid";
// comment
Vue.use(CommentGrid);
// comment;
Vue.use(VueFirestore, {
  key: "id", // the name of the property. Default is '.key'.
  enumerable: true, //  whether it is enumerable or not. Default is true.
});
require("firebase/firestore");
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(require("vue-moment"));
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
