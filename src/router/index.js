import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import firebase from "firebase";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",

    component: () => import("../views/About.vue"),
  },
  {
    path: "/admin",
    name: "admin",
    component: () => import("../components/admin.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../components/login.vue"),
  },

  {
    path: "/admin/inserts",
    name: "inserts",
    component: () => import("../components/admin/inserts.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/views",
    name: "views",
    component: () => import("../components/admin/views.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/:edit",
    name: "edit",
    component: () => import("../components/admin/edit.vue"),
  },
  {
    path: "/projects/:viewproject",
    name: "view",
    component: () => import("../views/view.vue"),
  },
  {
    path: "/web",
    name: "web",
    component: () => import("../views/web.vue"),
  },

  {
    path: "/db_user",
    name: "db_user",
    component: () => import("../components/db_user.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/login_register",
    name: "login_register",
    component: () => import("../components/users/page_login_register"),
  },
  {
    path: "/upload_infomation",
    name: "upload_infomation",
    component: () => import("../components/users/upload_infomation"),
  },
  {
    path: "/db_user/user_view",
    name: "user_view",
    component: () => import("../components/users/user_view"),
  },
  {
    path: "/db_user/:edit",
    name: "user_edit",
    component: () => import("../components/users/user_edit.vue"),
  },

  {
    path: "/book",
    name: "book",

    component: () => import("../views/book.vue"),
  },
  {
    path: "/course",
    name: "course",
    component: () => import("../views/course/course.vue"),
  },
  {
    path: "/course/video",
    name: "video",
    component: () => import("../views/course/detail_video.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to, from, next) => {
  if (to.matched.some((rec) => rec.meta.requiresAuth)) {
    let user = firebase.auth().currentUser;
    if (user) {
      //user signed in proceed to route
      next();
    } else {
      //no user signed in redict to login
      next({ name: "login" });
    }
  } else {
    next();
  }
});

export default router;
