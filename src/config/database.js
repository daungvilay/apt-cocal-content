const firebase = require("firebase");
require("firebase/firestore");
import "firebase/storage";
var firebaseConfig = {
  apiKey: "AIzaSyA3l9lPvM0OiNgKnjHg4wCsakv_89wqtTk",
  authDomain: "the-winner-1168c.firebaseapp.com",
  databaseURL: "https://the-winner-1168c.firebaseio.com",
  projectId: "the-winner-1168c",
  storageBucket: "the-winner-1168c.appspot.com",
  messagingSenderId: "593808282987",
  appId: "1:593808282987:web:82d6f0c6a244d3e912282e",
  measurementId: "G-VTZMZSDFG0",
};
// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export { fb, db };
